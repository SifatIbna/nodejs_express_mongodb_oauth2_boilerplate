var express = require("express");
var router = express.Router();
const Model = require("../models/Test");
const mongoose = require("../utils/mongoose");
const jwt = require("passport-jwt");
const Auth = require("../middlewares/Auth");

// Lazy Responder :)
function responder(res, err, data) {
  if (err || !data) {
    console.log({
      err,
      data,
    });
    res.status(400).send({
      err,
      data,
    });
  } else {
    console.log("Data: " + data);
    res.status(200).send(data);
  }
}

// Login
router.post("/login/", (req, res) => {
  console.log(JSON.stringify(req.body));
  Auth.authenticate(req, (err, data) => {
    responder(res, err, data);
  });
});

var arr = [
  {
    id: 0,
    name: "shifu",
    age: 00,
    location: "Dhaka",
  },
];

// C
// router.post('/',  (req, res) => {
router.post("/", (req, res) => {
  //   Model.createData(req.body, (err, data) => {
  //     responder(res, err, data);
  //   });

  var obj = {
    id: arr.length,
    name: req.body.name,
    age: req.body.age,
    location: req.body.location,
  };

  arr.push(obj);
  res.send(obj);
});

// Ra
router.get("/", (req, res) => {
  //   Model.getAllData(
  //     {},
  //     req.query["page"] ? req.query["page"] : 0,
  //     (err, data) => {
  //       responder(res, err, data);
  //     }
  //   );
  res.send(arr);
});

// R1
router.get("/:id", (req, res) => {
  //   Model.getOneData({ email: req.params["id"] }, (err, data) => {
  //     responder(res, err, data);
  //   });
  var found = false;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].id == req.params.id) {
      found = true;
      res.send(arr[i]);
      break;
    }
  }

  if (!found) {
    res.status(404).send({
      data: null,
    });
  }
});

// R1
router.get("/byid/:id", (req, res) => {
  //   Model.getOneData({ _id: req.params["id"] }, (err, data) => {
  //     responder(res, err, data);
  //   });
});

// U1
router.put("/:id", (req, res) => {
  //   delete req.body.email;
  //   Model.updateOneData({ _id: req.params.id }, req.body, (err, data) => {
  //     responder(res, err, data);
  //   });
  var obj = {
    ...arr[req.params.id],
    name: req.body.name,
    age: req.body.age,
    location: req.body.location,
  };

  arr[req.params.id] = obj;
  res.send(arr);
});

// D1
router.delete("/:id", (req, res) => {
  //   Model.removeOneData({ _id: req.params["id"] }, (err, data) => {
  //     responder(res, err, data);
  //   });

  arr = arr.filter((item, index) => {
    return item.id != req.params.id;
  });

  res.send(arr);
});

// Da
router.delete("/", (req, res) => {
  //   Model.removeAllData((err, data) => {
  //     responder(res, err, data);
  //   });
  arr = [];
  res.send(arr);
});

module.exports = router;
